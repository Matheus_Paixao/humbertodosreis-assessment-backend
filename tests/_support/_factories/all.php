<?php
use League\FactoryMuffin\Faker\Facade as Faker;

$fm->define(\App\Domain\Category::class)->setDefinitions([
    'name'      => Faker::sentence('2'),
    'code'      => Faker::ean8(),
]);

$fm->define(\App\Domain\Product::class)->setDefinitions([
    'sku' => Faker::ean8(),
    'name' => Faker::sentence('2'),
    'price' => Faker::randomFloat(2, 0, 999999),
    'quantity' => Faker::numberBetween(0, 100),
    'description' => Faker::text()
]);