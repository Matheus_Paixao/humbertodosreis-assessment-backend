<?php namespace Domain;

use App\Domain\Category;

class CategoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testNewCategory()
    {
        $data = [
            'id' => 1,
            'name' => 'a name',
            'code' => 'a code'
        ];

        $category = Category::fromArray($data);

        $this->assertEquals($data['id'], $category->getId());
        $this->assertEquals($data['name'], $category->getName());
        $this->assertEquals($data['code'], $category->getCode());
    }

    public function testCategoryNotCanHaveEmptyName()
    {
        $this->expectException(\Assert\InvalidArgumentException::class);

        $data = [
            'id' => 1,
            'name' => '',
            'code' => 'a code'
        ];

        $category = Category::fromArray($data);

        $category->setName(null);
    }

    public function testCategoryNotCanHaveEmptyCode()
    {
        $this->expectException(\Assert\InvalidArgumentException::class);

        $data = [
            'id' => 1,
            'name' => 'a name',
            'code' => ''
        ];

        $category = Category::fromArray($data);

        $category->setCode('');
    }
}