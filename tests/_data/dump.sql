SET FOREIGN_KEY_CHECKS=0;
TRUNCATE `products_x_categories`;
TRUNCATE `products`;
TRUNCATE `categories`;
SET FOREIGN_KEY_CHECKS=1;