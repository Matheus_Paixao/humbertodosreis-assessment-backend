DEFAULT_GOAL := help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-27s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

install:
	docker-compose up -d --build --force-recreate && \
	docker-compose exec app composer install	
	
migration:
	docker-compose exec app vendor/bin/phinx migrate && \
	docker-compose exec app vendor/bin/phinx migrate -e test

run-tests:
	docker-compose exec app vendor/bin/codecept run unit
	
prune:
	docker system prune -a -f --volumes
	
down:
	docker-compose down
	
run-importer:
	docker-compose exec app bin/console app:product-importer assets/import.csv
	
show-logs:
	docker-compose logs -f app