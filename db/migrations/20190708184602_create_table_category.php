<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCategory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('categories');
        $table->addColumn('name', 'string', ['limit' => 128])
            ->addColumn('code', 'string', ['limit' => 128])
            ->addIndex(['code'], ['unique' => true, 'name' => 'idx_categories_code'])
            ->create()
        ;
    }
}
