<?php
return [
    "paths" => [
        "migrations" => "db/migrations",
        "seeds" => "db/seeds"
    ],
    "environments" => [
        "default_migration_table" => "phinxlog",
        "default_database" => "dev",
        "dev" => [
            "adapter" => "mysql",
            "charset" => "utf8mb4",
            "host" => env('DB_HOST'),
            "name" => env('DB_NAME'),
            "user" => env('DB_USER'),
            "pass" => env('DB_PASSWORD'),
            "port" => 3306
        ],
        "test" => [
            "adapter" => "mysql",
            "charset" => "utf8mb4",
            "host" => env('DBTEST_HOST'),
            "name" => env('DBTEST_NAME'),
            "user" => env('DBTEST_USER'),
            "pass" => env('DBTEST_PASSWORD'),
            "port" => 3306
        ]
    ],
    "version_order" => "creation"
];