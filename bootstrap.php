<?php
use App\Console\ProductImporterCommand;
use App\Infra\Persistence\MySQL\MySQLProductRepository;
use App\Service\ProductService;
use DI\ContainerBuilder;
use App\Controller\DashboardController;
use App\Controller\ProductsController;
use App\Controller\CategoriesController;
use App\Domain\CategoryRepository;
use App\Domain\ProductRepository;
use App\Infra\Persistence\MySQL\MySQLCategoryRepository;
use League\Plates\Engine;
use Monolog\Handler\StreamHandler;
use Zend\Diactoros\Response;
use Monolog\Logger;
use function DI\create;
use function DI\get;

require_once __DIR__ . '/vendor/autoload.php';

$containerBuilder = new ContainerBuilder();
$containerBuilder->useAutowiring(false);
$containerBuilder->useAnnotations(false);
$containerBuilder->addDefinitions([
    'Db' => function () {
        $conn = new PDO(
            'mysql:dbname=' . env('DB_NAME')  . ';host=' . env('DB_HOST'),
            env('DB_USER'),
            env('DB_PASSWORD'),
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_COMPRESS => true,
                PDO::MYSQL_ATTR_MULTI_STATEMENTS => true

            ]
        );

        return $conn;
    },
    'Logger' => function () {
        $logger = new Logger('app');
        $logger->pushHandler(new StreamHandler('php://stdout', Logger::DEBUG));

        return $logger;
    },
    'Response' => function () {
        return new Response();
    },
    'Template' => function () {
        $templates = new Engine(__DIR__ . '/templates');
        return $templates;
    },

    /*
     * Mappers
     */
    CategoryRepository::class => create(MySQLCategoryRepository::class)
        ->constructor(get('Db')),
    ProductRepository::class => create(MySQLProductRepository::class)
        ->constructor(get('Db')),

    /*
     * Services
     */
    ProductService::class => create(ProductService::class)
        ->constructor(get(ProductRepository::class), get(CategoryRepository::class)),
    /*
     * Commands
     */
    ProductImporterCommand::class => create(ProductImporterCommand::class)
        ->method('setProductService', get(ProductService::class))
        ->method('setLogger', get('Logger')),

    /*
     * Controllers
     */
    // Dashboard
    DashboardController::class => create(DashboardController::class)
        ->constructor(get('Response'), get('Template'))
        ->method('setProductMapper', get(ProductRepository::class)),
    ProductsController::class => create(ProductsController::class)
        ->constructor(get('Response'), get('Template'))
        ->method('setProductMapper', get(ProductRepository::class))
        ->method('setCategoryMapper', get(CategoryRepository::class)),
    CategoriesController::class => create(CategoriesController::class)
        ->constructor(get('Response'), get('Template'))
        ->method('setCategoryMapper', get(CategoryRepository::class)),
]);

/** @noinspection PhpUnhandledExceptionInspection */
$container = $containerBuilder->build();
