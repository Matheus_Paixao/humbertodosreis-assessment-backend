<?php


namespace App\Console;

use App\Foundation\CsvReader;
use App\Service\ProductService;
use Psr\Log\LoggerAwareTrait;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductImporterCommand extends Command
{

    use LoggerAwareTrait;

    protected static $defaultName = 'app:product-importer';

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @return ProductService
     */
    public function getProductService(): ProductService
    {
        if (null === $this->productService) {
            throw new RuntimeException('Product service not defined');
        }

        return $this->productService;
    }

    /**
     * @param ProductService $productService
     * @return ProductImporterCommand
     */
    public function setProductService(ProductService $productService): ProductImporterCommand
    {
        $this->productService = $productService;
        return $this;
    }


    public function configure()
    {
        $this->setDescription('Import products from csv file')
            ->setHelp('Import products from csv file')
            ->addArgument('filename', InputArgument::REQUIRED, 'csv file path');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('filename');

        $this->logger->info('Start import');
        $this->logger->info('Reading file '. $filename);

        $reader = new CsvReader($filename, true);

        foreach ($reader->read() as $data) {
            list($name, $sku, $description, $quantity, $price, $categoriesCode) = $data;

            $this->logger->info(sprintf(
                "Read line: %s, %s, %s, %s, %s, %s",
                $name,
                $sku,
                $description,
                $quantity,
                $price,
                $categoriesCode
            ));

            $categoriesCode = empty($categoriesCode) ? [] : explode('|', $categoriesCode);

            try {
                $this->logger->info('Trying import product');

                $this->productService->importProducts([
                    'name' => $name,
                    'sku' => $sku,
                    'description' => trim($description),
                    'quantity' => (int)$quantity,
                    'price' => $price,
                    'categories' => $categoriesCode
                ]);

                $this->logger->info('Product was imported successfully');
            } catch (\Exception $e) {
                $this->logger->error('Error while trying import the product');
                $this->logger->debug($e);
            }
        }

        $this->logger->info('End. \o/');

        return 0;
    }
}