<?php


namespace App\Service;


use App\Domain\Category;
use App\Domain\CategoryRepository;
use App\Domain\Product;
use App\Domain\ProductRepository;

class ProductService
{

    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * ProductService constructor.
     * @param ProductRepository $productRepository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param array $data
     * @return array
     */
    public function importProducts(array $data)
    {
        $categories = [];

        foreach ($data['categories'] as $code) {
            $category = $this->categoryRepository->findByCode($code);

            if ($category === null) {
                $category = Category::fromArray([
                    'name' => $code,
                    'code' => $code
                ]);

                $this->categoryRepository->add($category);
            }

            $categories[] = $category;
        }

        $product = $this->productRepository->findBySku($data['sku']);

        if ($product === null) {
            $product = Product::fromArray([
                'name' => $data['name'],
                'sku' => $data['sku'],
                'description' => $data['description'],
                'quantity' => (int)$data['quantity'],
                'price' => (float)$data['price'],
                'categories' => $categories
            ]);

            $this->productRepository->add($product);
        } else {
            $product->setName($data['name'])
                ->setDescription($data['description'])
                ->setQuantity((int)$data['quantity'])
                ->setPrice((float)$data['price'])
                ->setCategories($categories);

            $this->productRepository->update($product);
        }
    }
}