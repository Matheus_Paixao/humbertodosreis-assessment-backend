<?php declare(strict_types=1);


namespace App\Controller;

use App\Domain\Category;
use App\Domain\CategoryRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Class CategoriesController
 * @package App
 */
class CategoriesController extends BaseController
{
    /**
     * @var CategoryRepository
     */
    private $categoryMapper;

    /**
     * @param CategoryRepository $categoryMapper
     */
    public function setCategoryMapper(CategoryRepository $categoryMapper)
    {
        $this->categoryMapper = $categoryMapper;
    }

    /**
     * @return CategoryRepository
     */
    public function getCategoryMapper()
    {
        if (null === $this->categoryMapper) {
            throw new RuntimeException('Category mapper not defined');
        }

        return $this->categoryMapper;
    }

    /**
     * @return ResponseInterface
     */
    public function list()
    {
        return $this->render('categories-list', [
            'categories' => $this->getCategoryMapper()->all()
        ]);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface|RedirectResponse
     */
    public function add(ServerRequestInterface $request)
    {
        if ($request->getMethod() == 'POST') {
            $data = $request->getParsedBody();

            $category = Category::fromArray([
               'name' => $data['name'],
               'code' => $data['code'],
            ]);

            $this->getCategoryMapper()->add($category);

            return new RedirectResponse('/categories?added=1');
        }

        return $this->render('categories-form', ['new' => true]);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface|RedirectResponse
     */
    public function edit(ServerRequestInterface $request)
    {
        $id = (int) $request->getAttribute('id');

        $category = $this->getCategoryMapper()->find($id);

        if ($request->getMethod() == 'POST') {
            $data = $request->getParsedBody();

            $category->setCode($data['code'])
                ->setName($data['name']);

            $this->getCategoryMapper()->update($category);

            return new RedirectResponse('/categories?edited=1');
        }

        return $this->render('categories-form', ['category' => $category, 'new' => false]);
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse
     */
    public function remove(ServerRequestInterface $request)
    {
        $id = (int) $request->getAttribute('id');

        $category = $this->getCategoryMapper()->find($id);

        $this->getCategoryMapper()->remove($category->getId());

        return new RedirectResponse('/categories?removed=1');
    }
}