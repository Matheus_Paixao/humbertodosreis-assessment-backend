<?php declare(strict_types=1);


namespace App\Controller;

use App\Domain\ProductRepository;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

class DashboardController extends BaseController
{
    /**
     * @var ProductRepository
     */
    private $productMapper;

    /**
     * @param ProductRepository $categoryMapper
     */
    public function setProductMapper(ProductRepository $categoryMapper)
    {
        $this->productMapper = $categoryMapper;
    }

    /**
     * @return ProductRepository
     */
    public function getProductMapper()
    {
        if (null === $this->productMapper) {
            throw new RuntimeException('Product mapper not defined');
        }

        return $this->productMapper;
    }

    /**
     * @return ResponseInterface
     */
    public function __invoke(): ResponseInterface
    {

        $totalProducts = $this->getProductMapper()->count();
        $products = $this->getProductMapper()->listRandomly();

        return $this->render('dashboard', [
            'totalProducts' => $totalProducts,
            'products' => $products
        ]);
    }
}