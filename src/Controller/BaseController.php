<?php declare(strict_types=1);

namespace App\Controller;


use Psr\Http\Message\ResponseInterface;
use League\Plates\Engine;

class BaseController
{

    /**
     * @var ResponseInterface
     */
    private $response;

    private $templates;

    /**
     * Dashboard constructor.
     * @param ResponseInterface $response
     * @param Engine $templates
     */
    public function __construct(
        ResponseInterface $response,
        Engine $templates
    )
    {
        $this->response = $response;
        $this->templates = $templates;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * @return Engine
     */
    public function getTemplate(): Engine
    {
        return $this->templates;
    }

    /**
     * @param $view
     * @param array $data
     * @return ResponseInterface
     */
    public function render($view, array $data = []): ResponseInterface
    {
        $response = $this->response->withHeader('Content-Type', 'text/html');
        $response->getBody()
            ->write($this->templates->render($view, $data));

        return $response;
    }
}