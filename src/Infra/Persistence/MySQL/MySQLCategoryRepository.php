<?php declare(strict_types=1);


namespace App\Infra\Persistence\MySQL;

use App\Domain\Category;
use App\Domain\CategoryRepository;
use PDO;

class MySQLCategoryRepository implements CategoryRepository
{

    /**
     * @var PDO
     */
    private $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @inheritDoc
     */
    public function all(): array
    {
        $collection = [];

        $stmt = $this->connection->query('SELECT id, name, code FROM categories');

        foreach ($stmt as $row) {
            $collection[] = Category::fromArray($row);
        }

        return $collection;
    }

    /**
     * @inheritDoc
     */
    public function add(Category $category): void
    {
        $stmt = $this->connection->prepare('INSERT categories(`name`, `code`) VALUES (?, ?)');
        $stmt->bindValue(1, $category->getName());
        $stmt->bindValue(2, $category->getCode());
        $stmt->execute();

        $lastInsertId = $this->connection->lastInsertId();

        $category->setId((int) $lastInsertId);
    }

    /**
     * @inheritDoc
     */
    public function update(Category $category): void
    {
        $this->connection
            ->prepare('UPDATE categories SET name=?, code=? WHERE id=?')
            ->execute([
                $category->getName(),
                $category->getCode(),
                $category->getId()
            ]);
    }

    /**
     * @inheritDoc
     */
    public function remove(int $id): void
    {
        $this->connection->prepare('DELETE FROM categories WHERE id=?')
            ->execute([$id]);
    }


    /**
     * @inheritDoc
     */
    public function find(int $id): ?Category
    {
        $stmt = $this->connection->prepare('SELECT id, name, code FROM categories WHERE id=?');
        $stmt->bindValue(1, $id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (empty($row)) {
            return null;
        }

        return Category::fromArray($row);
    }

    /**
     * @inheritDoc
     */
    public function findByCode(string $code): ?Category
    {
        $stmt = $this->connection->prepare('SELECT id, name, code FROM categories WHERE code=?');
        $stmt->bindValue(1, $code);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (empty($row)) {
            return null;
        }

        return Category::fromArray($row);
    }
}