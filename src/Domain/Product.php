<?php


namespace App\Domain;


use Assert\Assertion;

class Product
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $sku;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @var int
     */
    private $quantity = 0;

    /**
     * @var array
     */
    private $categories = [];

    /**
     * @var string
     */
    private $description;

    /**
     * @param array $data
     * @return Product
     */
    static public function fromArray(array $data): Product
    {
        $product = new self;

        if (isset($data['id'])) {
            $product->setId((int)$data['id']);
        }

        $product->setSku($data['sku'])
            ->setName($data['name'])
            ->setPrice($data['price'])
            ->setQuantity($data['quantity'])
            ->setCategories($data['categories'])
            ->setDescription($data['description']);

        return $product;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return Product
     */
    public function setId(int $id): Product
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     * @return Product
     */
    public function setSku(string $sku): Product
    {
        Assertion::notEmpty($sku, 'SKU is required');

        $this->sku = $sku;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Product
     */
    public function setName(string $name): Product
    {
        Assertion::notEmpty($name, 'Name is required');

        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Product
     */
    public function setPrice(float $price): Product
    {
        Assertion::float($price, 'Price should be a float');

        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Product
     */
    public function setQuantity(int $quantity): Product
    {
        Assertion::integer($quantity, 'Quantity should be integer');
        Assertion::min($quantity, 0, 'Quantity can not be negative');

        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     * @return Product
     */
    public function setCategories(array $categories): Product
    {
        Assertion::allIsInstanceOf(
            $categories,
            Category::class,
            'Categories should be an array of objects of the type category'
        );

        $this->categories = $categories;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Product
     */
    public function setDescription(string $description): Product
    {
        $this->description = $description;
        return $this;
    }
}